# OpenML dataset: doa_bwin

https://www.openml.org/d/45711

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This analytic dataset contains actual betting behavior virtual live action betting for each participant aggregated over one month period from the date the account was opened. Also, it contains live action betting for each participant aggregated over the twenty-four-month study period from February 1, 2005 through February 28, 2007 or until the account was closed.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45711) of an [OpenML dataset](https://www.openml.org/d/45711). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45711/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45711/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45711/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

